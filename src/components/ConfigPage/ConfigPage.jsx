/* eslint-disable react/prop-types */
import React from 'react';
import DownloadText from './DownloadText/DownloadText';
import DownloadSvg from './DownloadSvg/DownloadSvg';
import ConfigSize from './ConfigSize/ConfigSize';
import ConfigMargin from './ConfigMargin/ConfigMargin';
import ConfigTextSize from './ConfigTextSize/ConfigTextSize';
import ConfigColor from './ConfigColor/ConfigColor';

const ConfigPage = ({
  getName, getText, getData, getSize, setSize, getMargin,
  setMargin, getTextSize, setTextSize, getColor, setColor,
}) => (
  <section className="about">
    <h1>Configuration</h1>
    <h2>Mindmap settings</h2>
    <ConfigSize
      getSize={getSize}
      setSize={setSize}
    />
    <ConfigMargin
      getMargin={getMargin}
      setMargin={setMargin}
    />
    <ConfigTextSize
      getTextSize={getTextSize}
      setTextSize={setTextSize}
    />
    <ConfigColor
      getColor={getColor}
      setColor={setColor}
    />
    <h2>Download</h2>
    <DownloadText
      getName={getName}
      getText={getText}
    />
    <DownloadSvg
      getName={getName}
      getData={getData}
      getSize={getSize}
      getMargin={getMargin}
      getTextSize={getTextSize}
    />
  </section>
);

export default ConfigPage;
