/* eslint-disable react/prop-types */
import React from 'react';

const ConfigColor = ({ getColor, setColor }) => {
  const handleColorChange = (e) => {
    setColor(e.target.type === 'checkbox' ? e.target.checked : e.target.value);
  };

  return (
    <div>
      Color scheme
      <select className="about__color" value={getColor} onChange={handleColorChange}>
        <option value="#242582 #553D67 #F64C72 #99738E #2F2FA2">Bright Accent Colors</option>
        <option value="#421d63">Purple</option>
        <option value="#CCCCCC">Grey</option>
        <option value="#05386b #379683 #5CDB95 #8EE4AF #3D787C">Cool and Fresh</option>
        <option value="#8FD8D2 #FEDCD2 #DF744A #DCB239">Pretty pastels</option>
        <option value="#E24E42 #E9B000 #EB6E80 #008F95">Bold and punchy</option>
        <option value="#0B3C5D #328CC1 #D9B310 #1D2731">Elegant and sophisticated</option>
        <option value="#4CDEF5 #A4D555 #FF5992 #841983">Fresh and lively</option>
        <option value="#68BAA7 #FBA100 #6C648B #B6A19E">Cool and calm</option>
        <option value="#945D60 #626E60 #AF473C #3C3C3C">Earthy and fresh</option>
        <option value="#7CDBD5 #F53240 #F9BE02 #02C8A7">Warm and wonderful</option>
        <option value="#2C3531 #116466 #D9B08C #FFCB9A #D1E8E2">Sleek and Futuristic</option>
        <option value="#2F4454 #2E151B #DA7B93 #376E6F #1C3334">Texturized and Dynamic</option>
        <option value="#88BDBC #254E58 #112D32 #4F4A41 #6E6658">Corporate and Traditional</option>
        <option value="#25274D #464766 #AAABB8 #2E9CCA #29648A">Blue and Refreshing</option>
        <option value="#24305E #F76C6C #F8E9A1 #A8D0E6 #374785">Vibrant and Elegant</option>
        <option value="#D6CE15 #A4A71E #53900F #1F6521 #1F2605">Striking Citrus Colors</option>
        <option value="#080F5B #0D19A3 #15DB95 #F4E4C1 #E4C580">Audacious and Unique</option>
        <option value="#19181A #479761 #CEBC81 #A16E83 #B19F9E">Modern and Minimalist</option>
        <option value="#265077 #022140 #494B68 #1E4258 #2D5F5D">Corporate and Serious</option>
      </select>
    </div>
  );
};

export default ConfigColor;
