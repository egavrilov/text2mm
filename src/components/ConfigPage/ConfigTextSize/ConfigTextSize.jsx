/* eslint-disable react/prop-types */
import React from 'react';

const ConfigTextSize = ({ getTextSize, setTextSize }) => {
  const handleTextSizeChange = (e) => {
    setTextSize(e.target.value);
  };

  return (
    <div>
      Text Size
      <input type="range" min="4" max="100" value={getTextSize} className="about__slider" onChange={handleTextSizeChange} />
    </div>
  );
};

export default ConfigTextSize;
